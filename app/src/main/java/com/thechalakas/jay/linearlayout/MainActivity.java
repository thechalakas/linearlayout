package com.thechalakas.jay.linearlayout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/*
 * Created by jay on 15/09/17. 11:38 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 * https://bitbucket.org/account/user/thechalakas/projects/DROID
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
